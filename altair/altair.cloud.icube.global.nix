# physical specification for none backend

{
  altair =
  { config, lib, pkgs, resources, ...}:
  { deployment.targetEnv = "none";
    deployment.targetHost = "145.239.64.166";

    # Workaround for error: [Errno 7] Argument list too long
    deployment.hasFastConnection = true;
  };
}


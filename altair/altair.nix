{
  network.description = "altair.cloud.icube.global";

  # Keep a GC root for the build
  network.enableRollback = true;

  altair =
  { config, lib, pkgs, ... }:
  {
    imports = [
      ./config
    ];
  };
}

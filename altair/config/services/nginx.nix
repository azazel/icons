{ config, lib, pkgs, ... }:
let
  nextcloudPath = "/mnt/var/nextcloud";
  hostname = "repository";
in {
  enable = true;
  virtualHosts."${hostname}.${config.networking.domain}" = {
    forceSSL = true;
    enableACME = true;
    root = "${pkgs.nextcloud}";
  };
}

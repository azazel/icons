{ config, lib, pkgs, ... }:

{
  services.openssh.enable = true;  
  services.postfix = import ./postfix.nix { inherit config lib pkgs; };
  services.nginx = import ./nginx.nix { inherit config lib pkgs; };
}

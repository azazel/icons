{ config, lib, pkgs, ... }:

{
  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.azazel = {
    isNormalUser = true;
    uid = 1000;
  };

  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCoWtmwzsREPJ6kN8oSB8nyfGbE8wY9O6OmUqsZwtAViOcH27fzPpw6oKhXfXEeSVhcQlk3ffa38+KAhewhOTjW2skASuA7lhPTNF31N1dD+ZPQxWc7SCWsmtjhK9KySsd/clHIMRJqpvy7hQHgUTuTPqNE136er3LNyDaxCv1rx1qJBmlN/1PqZhgNybhnz3VCh59PqSeYqy+hkREXENdnx9xOq8zb3wclVw/b5c/l0YNq3Qk4HFa57r8iAcuZjItuwjANfh08UeEtCNuO0Ap1XChaXeFAw7P6pLORFaxjE7wbxSVfuGxxysLKWy8onebLqfqjCGphpUzvIKVMdQBFOf8a/vNLHbeiICpJ4l6dvJ+Omkpf6WDamepCbT3xftDLOzNTbqgToDPKq+Lt66etqqKqkaN+RKTgbUSYaJZWUTnPNKDjLYWWDKB2cXaFXInRcbLoDQy1jij9xanZWVEyOlTt3xE/maskagq+pvo4696+9U6T+KhJ8DXdbO6anOGZQxGD7cR+IMA3dUEcy4SZKp1R/xHvy0Fylj31ivyqNuyYybEN5IOZq7AcWnFJXkTYELfjbU14VAVlSjgBmcBy6ikkIsSwZ3R4Q5vs77d9+JpGF7oNHHNU1jrGMgFoy8QlqDQNEXzbG9+dcwtUR5jJ4Opp8Am2H3yxj3IjcsMDbw== azazel@ender"
  ];
}

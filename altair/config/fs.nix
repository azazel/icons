{ config, lib, pkgs, ... }:

{
  fileSystems."/" =
  { device = "/dev/disk/by-uuid/ffa0ebab-503e-477a-8de3-3c43ab111ef4";
    fsType = "ext4";
  };

  fileSystems."/mnt/tank" =
  { device = "tank";
    fsType = "zfs";
  };

  fileSystems."/nix" =
  { device = "tank/nix";
    fsType = "zfs";
  };

  fileSystems."/boot/efi" =
  { device = "/dev/disk/by-uuid/CE01-3DCD";
    fsType = "vfat";
  };

}

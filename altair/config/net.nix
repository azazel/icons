{ config, lib, pkgs, ... }:

{
  networking.hostName = "altair"; # Define your hostname.
  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 22 ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall.enable = true;
  networking.enableIPv6 = false;
  networking.domain = "cloud.icube.global";
  networking.defaultGateway = "145.239.64.254";
  networking.nameservers = [ "213.186.33.99" "8.8.8.8" ];
  networking.interfaces.eth0 = {
    ipAddress = "145.239.64.166";
    prefixLength = 24;
  };
  # required by zfs
  networking.hostId = "7dce88ce";
}

RELEASE="17.09"
NIXPKGS="https://github.com/NixOS/nixpkgs-channels/archive/nixos-${RELEASE}.tar.gz"
NIX_PATH="nixpkgs=${NIXPKGS}"

export NIX_PATH

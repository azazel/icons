.. -*- coding: utf-8 -*-
.. :Project:   icons teamsites -- readme
.. :Created:   sab 20 gen 2018 20:31:04 CET
.. :Author:    Alberto Berti <alberto@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2018 Alberto Berti
..

=================
 iCons Teamsites
=================

These are the deployments for iCons Foundation. They are done using
nixops__ and nixos__.

Do not forget to source the ``env.sh`` file.

__ https://nixos.org/nixops/manual
__ https://nixos.org/nixos/manual/
